<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190327153853 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Generate event sourcing tables';
    }

    public function up(Schema $schema) : void
    {
        $sql = <<<SQL
            CREATE TABLE events (
                id serial,
                aggregate_id UUID not null,
                event_name varchar(250),
                payload json,
                created timestamp,
                version BIGINT,
                category varchar(250),
                CONSTRAINT events_pk UNIQUE (aggregate_id, version) 
            )
SQL;
        $this->addSql($sql);

        $sql = <<<SQL
            CREATE INDEX events_category ON events(category);
SQL;
        $this->addSql($sql);

        $sql = <<<SQL
            CREATE INDEX events_aggregate_id ON events(aggregate_id);
SQL;
        $this->addSql($sql);
    }

    public function down(Schema $schema) : void
    {
        $this->throwIrreversibleMigrationException();
    }
}
