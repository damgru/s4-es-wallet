<?php

namespace App\Common\Aggregate;

abstract class AggregateChanged
{
    /** @var string */
    protected $aggregateId;

    /** @var array */
    protected $payload = [];

    /** @var \DateTimeImmutable */
    protected $created;

    /** @var int */
    protected $version;

    /** @var string */
    protected $category;

    /**
     * AggregateChanged constructor.
     * @param string $aggregateId
     * @param array $payload
     * @param \DateTimeImmutable $created
     * @param int $version
     * @param string $category
     */
    public function __construct(string $aggregateId, array $payload = [], \DateTimeImmutable $created = null, int $version = 0, string $category = '')
    {
        $this->aggregateId = $aggregateId;
        $this->payload = $payload;
        try {
            $this->created = empty($created) ? new \DateTimeImmutable() : $created;
        } catch (\Exception $e) {
            $this->created = null;
        }
        $this->version = $version;
        $this->category = $category;
    }

    abstract public function eventName(): string;

    public function aggregateId(): string
    {
        return $this->aggregateId;
    }

    public function payload() : array
    {
        return $this->payload;
    }

    public function version() : int
    {
        return $this->version;
    }

    public function withVersion(int $version) : self
    {
        $this->version = $version;
        return $this;
    }

    public function created() : \DateTimeImmutable
    {
        return $this->created;
    }

    public function category() : string
    {
        return $this->category;
    }
}