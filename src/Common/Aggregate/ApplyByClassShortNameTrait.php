<?php
/**
 * Created by PhpStorm.
 * User: dgrudzien
 * Date: 19.03.19
 * Time: 10:44
 */

namespace App\Common\Aggregate;

use App\Common\Aggregate\AggregateChanged;
use App\Wallet\Domain\DomainException;
use ReflectionClass;

trait ApplyByClassShortNameTrait
{
    protected function apply(AggregateChanged $event) : void
    {
        $method = $this->resolveEventHandlerMethod($event);

        if(!method_exists($this, $method)){
            return;
        }

        $this->$method($event);
    }


    /**
     * @param AggregateChanged $event
     * @return string
     * @throws DomainException
     */
    protected function resolveEventHandlerMethod(AggregateChanged $event)
    {
        try {
            return 'apply' . ((new ReflectionClass($event))->getShortName());
        } catch (\ReflectionException $e) {
            throw new DomainException('no event class found');
        }
    }

}