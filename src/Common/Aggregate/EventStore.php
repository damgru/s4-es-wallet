<?php
/**
 * Created by PhpStorm.
 * User: dgrudzien
 * Date: 01.03.19
 * Time: 10:58
 */

namespace App\Common\Aggregate;

use App\Common\Uuid;

interface EventStore
{
    /**
     * @param AggregateChanged[] $events
     */
    public function add(AggregateChanged ...$events) : void ;

    /**
     * @param \App\Common\Uuid $aggregateId
     * @return iterable<AggregateChanged>
     */
    public function getEvents(Uuid $aggregateId) : iterable;

    /**
     * @return iterable<AggregateChanged>
     */
    public function getAllEvents() : iterable;

    /**
     * @param Uuid $aggregateId
     * @param int $version
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsFromVersion(Uuid $aggregateId, int $version, $limit = 0) : iterable;
}