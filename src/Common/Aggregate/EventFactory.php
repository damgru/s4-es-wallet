<?php

namespace App\Common\Aggregate;

use App\Common\Aggregate\AggregateChanged;

interface EventFactory
{
    public function createFromName(
        string $eventName,
        string $aggregateId,
        array $payload,
        \DateTimeImmutable $created = null,
        int $version = 0,
        string $category
    ) : AggregateChanged;
}