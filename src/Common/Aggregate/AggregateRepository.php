<?php
/**
 * Created by PhpStorm.
 * User: dgrudzien
 * Date: 01.03.19
 * Time: 14:51
 */

namespace App\Common\Aggregate;

use App\Common\Uuid;

abstract class AggregateRepository
{
    /** @var EventStore */
    private $eventStore;

    /**
     * AggregateRepository constructor.
     * @param EventStore $eventStore
     */
    public function __construct(EventStore $eventStore)
    {
        $this->eventStore = $eventStore;
    }

    protected function saveAggregateRoot(AggregateRoot $aggregateRoot)
    {
        $events = $aggregateRoot->getUncommittedEvents();
        foreach ($events as $event)
        {
            $this->eventStore->add($event);
        }
        $aggregateRoot->markEventsAsCommitted();
    }

    protected function loadAggregateRoot($class, Uuid $aggregateId): AggregateRoot
    {
        if(!$this->isAggregateRootClass($class)){
            throw new AggregateException('You cannot load non AggregateRoot object');
        }

        $events = $this->eventStore->getEvents($aggregateId);
        return call_user_func($class.'::fromEvents', $events);
    }

    private function isAggregateRootClass($class) : bool
    {
        try {
            $reflection = new \ReflectionClass($class);
            return $reflection->isSubclassOf(AggregateRoot::class);
        }
        catch (\ReflectionException $e) {
            return false;
        }
    }
}