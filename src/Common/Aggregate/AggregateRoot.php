<?php
/**
 * Created by PhpStorm.
 * User: Cainon
 * Date: 28.02.2019
 * Time: 22:00
 */

namespace App\Common\Aggregate;

use App\Wallet\Domain\DomainException;
use ReflectionClass;

abstract class AggregateRoot
{
    /** @var int current version*/
    protected $version = 0;

    /** @var AggregateChanged[] */
    protected $events = [];

    /**
     * @return AggregateChanged[]
     */
    public function getUncommittedEvents(): array
    {
        return $this->events;
    }

    public function markEventsAsCommitted(): void
    {
        $this->events = [];
    }

    /**
     * @param AggregateChanged $event
     * @throws DomainException
     */
    protected function record(AggregateChanged $event) : void
    {
        $this->version++;
        $this->events[] = $event->withVersion($this->version);
        $this->apply($event);
    }

    /**
     * @param AggregateChanged $event
     * @throws DomainException
     */
    abstract protected function apply(AggregateChanged $event) : void;

    abstract protected function aggregateId() : string ;

    /**
     * @param iterable<AggregateChanged> $historyEvents
     * @throws DomainException
     */
    protected function replay(iterable $historyEvents)
    {
        foreach ($historyEvents as $event)
        {
            /** @var $event AggregateChanged */
            $this->version++;
            $this->apply($event);
        }
    }

    /**
     * @param iterable $events
     * @return AggregateRoot
     */
    public static function fromEvents(iterable $events) : AggregateRoot
    {
        $obj = new static();
        $obj->replay($events);
        return $obj;
    }

    public function __toString()
    {
        return get_class($this) . "({$this->aggregateId()})";
    }

}