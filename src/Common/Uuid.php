<?php

namespace App\Common;

use Ramsey\Uuid\UuidInterface;

class Uuid
{
    /** @var UuidInterface */
    private $ramseyUuid;

    /**
     * Uuid constructor.
     * @param $string
     */
    private function __construct(UuidInterface $ramseyUuid)
    {
        $this->ramseyUuid = $ramseyUuid;
    }

    /**
     * @return Uuid
     * @throws \Exception
     */
    public static function generate() : self
    {
        return new self(\Ramsey\Uuid\Uuid::uuid4());
    }

    public static function fromString($string) : self
    {
        return new self(\Ramsey\Uuid\Uuid::fromString($string));
    }

    public function toString() : string
    {
        return $this->ramseyUuid->toString();
    }

    public function __toString()
    {
        return $this->toString();
    }
}