<?php

namespace App\Common;

use PHPUnit\Framework\TestCase;

class UuidTest extends TestCase
{
    public function testGenerate()
    {
        $uuid = Uuid::generate();
        $this->assertInstanceOf(Uuid::class, $uuid);
    }

    public function testToString()
    {
        $string = 'af5b2216-1a4e-4794-8d89-c8740e6bfebd';
        $uuid = Uuid::fromString($string);
        $this->assertEquals($string, $uuid->toString());
    }

    public function testUnderscoreToString()
    {
        $string = 'af5b2216-1a4e-4794-8d89-c8740e6bfebd';
        $uuid = Uuid::fromString($string);
        $this->assertEquals($string, $uuid->__toString());
    }
}
