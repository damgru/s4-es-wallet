<?php

namespace App\Wallet\CommandHandler;

use App\Common\Uuid;
use App\Wallet\Command\CreateWalletCommand;
use App\Wallet\Command\RenameWalletCommand;
use App\Wallet\Domain\Wallet\Wallet;
use App\Wallet\Domain\Wallet\WalletName;
use App\Wallet\Domain\Wallet\WalletRepository;

class RenameWalletHandler
{
    private $walletRepo;

    /**
     * CreateWalletHandler constructor.
     * @param WalletRepository $walletRepo
     */
    public function __construct(WalletRepository $walletRepo)
    {
        $this->walletRepo = $walletRepo;
    }

    /**
     * @param RenameWalletCommand $command
     */
    public function __invoke(RenameWalletCommand $command)
    {
        $wallet = $this->walletRepo->getById(Uuid::fromString($command->getWalletId()));
        $wallet->renameTo(new WalletName($command->getNewName()));
        $this->walletRepo->save($wallet);
    }
}