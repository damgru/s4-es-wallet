<?php

namespace App\Wallet\CommandHandler;

use App\Wallet\Command\CreateWalletCommand;
use App\Wallet\Domain\Wallet\Wallet;
use App\Wallet\Domain\Wallet\WalletName;
use App\Wallet\Domain\Wallet\WalletRepository;

class CreateWalletHandler
{
    private $walletRepo;

    /**
     * CreateWalletHandler constructor.
     * @param WalletRepository $walletRepo
     */
    public function __construct(WalletRepository $walletRepo)
    {
        $this->walletRepo = $walletRepo;
    }

    /**
     * @param CreateWalletCommand $command
     */
    public function __invoke(CreateWalletCommand $command)
    {
        $wallet = Wallet::create($command->uuid, new WalletName($command->name));
        $this->walletRepo->save($wallet);
    }
}