<?php

namespace App\Wallet\CommandHandler;

use App\Wallet\Command\CreateProposalCommand;
use App\Wallet\Domain\Proposal\Proposal;
use App\Wallet\Domain\Wallet\ProposalRepository;
use App\Wallet\Domain\Wallet\WalletRepository;

class CreateProposalHandler
{
    private $repo;

    /**
     * CreateWalletHandler constructor.
     * @param WalletRepository $repo
     */
    public function __construct(ProposalRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @param CreateProposalCommand $command
     */
    public function __invoke(CreateProposalCommand $command)
    {
        $proposal = Proposal::createNewProposal(
            $command->getUuid(),
            $command->getWalletUuid(),
            $command->getAmount(),
            $command->getDesc()
        );

        $this->repo->save($proposal);
    }
}