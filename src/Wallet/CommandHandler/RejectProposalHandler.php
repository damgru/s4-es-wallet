<?php

namespace App\Wallet\CommandHandler;

use App\Wallet\Domain\Wallet\ProposalRepository;

class RejectProposalHandler
{
    private $repo;

    /**
     * CreateWalletHandler constructor.
     */
    public function __construct(ProposalRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @param RejectProposalCommand $command
     * @throws \App\Wallet\Domain\Proposal\ProposalAlreadyAcceptedException
     * @throws \App\Wallet\Domain\Proposal\ProposalAlreadyRejectedException
     */
    public function __invoke(RejectProposalCommand $command)
    {
        $proposal = $this->repo->getById($command->getUuid());
        $proposal->reject($command->reason());
        $this->repo->save($proposal);
    }
}