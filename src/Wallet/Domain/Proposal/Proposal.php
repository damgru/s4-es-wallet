<?php

namespace App\Wallet\Domain\Proposal;

use App\Common\Aggregate\AggregateRoot;
use App\Common\Aggregate\ApplyByClassShortNameTrait;
use App\Common\Uuid;
use App\Wallet\Domain\Proposal\Event\ProposalAccepted;
use App\Wallet\Domain\Proposal\Event\ProposalCreated;
use App\Wallet\Domain\Proposal\Event\ProposalRejected;

final class Proposal extends AggregateRoot
{
    use ApplyByClassShortNameTrait;

    /** @var Uuid */
    private $uuid;
    /** @var Uuid */
    private $walletUuid;
    /** @var string todo: czy to wywaic stad? */
    private $desc;
    /** @var float */
    private $amount;

    private $isRejected = false;
    private $rejectReason;
    /** @var bool */
    private $isAccepted;

    public static function createNewProposal(Uuid $uuid, Uuid $walletUuid, float $amount, string $description)
    {
        $proposal = new static();
        $proposal->record(ProposalCreated::with($uuid, $walletUuid, $amount, $description));
        return $proposal;
    }

    public function reject(string $reason = '')
    {
        if ($this->isRejected) {
            throw new ProposalAlreadyRejectedException($this->rejectReason);
        }

        if ($this->isAccepted) {
            throw new ProposalAlreadyAcceptedException();
        }

        $this->record(ProposalRejected::with($this->aggregateId(), $reason));
    }

    public function accept()
    {
        if ($this->isRejected) {
            throw new ProposalAlreadyRejectedException($this->rejectReason);
        }

        if ($this->isAccepted) {
            throw new ProposalAlreadyAcceptedException();
        }

        $this->record(ProposalAccepted::with($this->aggregateId()));
    }

    protected function aggregateId(): string
    {
        return $this->uuid->__toString();
    }

    public function walletUuid(): Uuid
    {
        return $this->walletUuid;
    }

    public function amount()
    {
        return $this->amount;
    }

    protected function applyProposalCreated(ProposalCreated $event)
    {
        $this->uuid = Uuid::fromString($event->aggregateId());
        $this->walletUuid = $event->walletUuid();
        $this->desc = $event->description();
        $this->amount = $event->amount();
    }

    protected function applyProposalRejected(ProposalRejected $event)
    {
        $this->isRejected = true;
        $this->rejectReason = $event->reason();
    }

    protected function applyProposalAccepted(ProposalAccepted $event)
    {
        $this->isAccepted = true;
    }


}