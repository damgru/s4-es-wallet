<?php


namespace App\Wallet\Domain\Proposal\Event;


use App\Common\Aggregate\AggregateChanged;

class ProposalRejected extends AggregateChanged
{
    public static function with(string $aggregateId, string $reason)
    {
        return new static($aggregateId, [
            'reason' => $reason
        ]);
    }

    public function eventName(): string
    {
        return 'proposal-rejected';
    }

    public function reason(): string
    {
        return $this->payload['reason'];
    }


}