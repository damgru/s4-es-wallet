<?php

namespace App\Wallet\Domain\Proposal\Event;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Uuid;

class ProposalCreated extends AggregateChanged
{
    /**
     * @return string
     */
    public function eventName(): string
    {
        return 'proposal-created';
    }

    /**
     * @param Uuid $uuid
     * @param Uuid $walletUuid
     * @param float $amount
     * @param string $description
     * @return ProposalCreated
     */
    public static function with(\App\Common\Uuid $uuid, \App\Common\Uuid $walletUuid, float $amount, string $description)
    {
        return new static($uuid, [
            'wallet_uuid' => $walletUuid->toString(),
            'amount' => $amount,
            'desc' => $description
        ]);
    }

    /**
     * @return Uuid
     */
    public function walletUuid()
    {
        return Uuid::fromString($this->payload['wallet_uuid']);
    }

    /**
     * @return float
     */
    public function amount()
    {
        return $this->payload['amount'];
    }

    /**
     * @return string
     */
    public function description()
    {
        return $this->payload['desc'];
    }

}