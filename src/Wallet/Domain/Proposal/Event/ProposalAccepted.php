<?php


namespace App\Wallet\Domain\Proposal\Event;

use App\Common\Aggregate\AggregateChanged;

class ProposalAccepted extends AggregateChanged
{
    public static function with(string $aggregateId)
    {
        return new static($aggregateId);
    }

    public function eventName(): string
    {
        return 'proposal-accepted';
    }

}