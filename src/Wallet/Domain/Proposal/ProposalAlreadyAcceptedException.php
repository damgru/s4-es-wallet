<?php


namespace App\Wallet\Domain\Proposal;

use App\Wallet\Domain\DomainException;

class ProposalAlreadyAcceptedException extends DomainException
{

    /**
     * ProposalAlreadyAcceptedException constructor.
     */
    public function __construct()
    {
    }
}