<?php


namespace App\Wallet\Domain\Proposal;

use App\Wallet\Domain\DomainException;
use Throwable;

class ProposalAlreadyRejectedException extends DomainException
{
    public function __construct(string $reason = "", array $debugData = [], Throwable $previous = null)
    {
        $message = "Proposal is Already Rejeted with reason '$reason'";
        parent::__construct($message, $debugData, $previous);
    }
}