<?php

namespace App\Wallet\Domain\Proposal;

use App\Common\Uuid;

interface ProposalRepository
{
    public function getById(Uuid $uuid): Proposal;

    public function save(Proposal $proposal): void;
}