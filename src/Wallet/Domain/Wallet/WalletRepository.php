<?php

namespace App\Wallet\Domain\Wallet;


use App\Common\Uuid;

interface WalletRepository
{
    public function getById(Uuid $walletId) : Wallet;
    public function save(Wallet $wallet) : void;
}