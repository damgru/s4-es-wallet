<?php

namespace App\Wallet\Domain\Wallet;

use App\Wallet\Domain\DomainException;

class WalletName
{
    private $name;

    /**
     * WalletName constructor.
     * @param $name
     * @throws DomainException
     */
    public function __construct($name)
    {
        if(!$this->isValid($name)) {
            throw new DomainException("Wallet Name $name is invalid");
        }
        $this->name = $name;
    }

    private function isValid($name)
    {
        return !empty($name)
            && !strpbrk($name, '!@#$%^&*()+={}[];:\'"<>?/.,')
            && trim($name) == $name;
    }

    public function toString() : string
    {
        return $this->name;
    }


}