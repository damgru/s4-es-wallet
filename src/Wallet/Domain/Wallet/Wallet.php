<?php

namespace App\Wallet\Domain\Wallet;

use App\Common\Aggregate\AggregateRoot;
use App\Common\Aggregate\ApplyByClassShortNameTrait;
use App\Common\Uuid;
use App\Wallet\Domain\Wallet\Event\AddedMoneyToWallet;
use App\Wallet\Domain\Wallet\Event\WalletCreated;
use App\Wallet\Domain\Wallet\Event\WalletRenamed;
use App\Wallet\Domain\Wallet\Event\WithdrawnMoneyFromWallet;

class Wallet extends AggregateRoot
{
    use ApplyByClassShortNameTrait;

    /** @var Uuid */
    private $walletId;

    /** @var float */
    private $balance = 0;

    /** @var WalletName */
    private $name;

    /**
     * @param \App\Common\Uuid $walletId
     * @param WalletName $walletName
     * @return Wallet
     * @throws \App\Wallet\Domain\DomainException
     */
    public static function create(Uuid $walletId, WalletName $walletName) : self
    {
        $wallet = new self();
        $wallet->record(WalletCreated::with($walletId, $walletName->toString()));
        return $wallet;
    }

    /**
     * @param WalletName $newName
     * @throws \App\Wallet\Domain\DomainException
     */
    public function renameTo(WalletName $newName)
    {
        $this->record(WalletRenamed::with($this->walletId, $newName->toString()));
    }

    /**
     * @param $amount
     * @throws \App\Wallet\Domain\DomainException
     */
    public function addMoney($amount)
    {
        // todo: validate
        if ($this->balance > 1000) {
            $this->record(BogatyTenZiomekJest::with($this->walletId, $this->balance));
        }

        $this->record(AddedMoneyToWallet::with($this->walletId, $amount));
    }

    protected function aggregateId(): string
    {
        return $this->walletId->toString();
    }

    /**
     * @return \App\Common\Uuid
     */
    public function walletId(): Uuid
    {
        return $this->walletId;
    }

    /**
     * @return float
     */
    public function balance(): float
    {
        return $this->balance;
    }

    /**
     * @return WalletName
     */
    public function name(): WalletName
    {
        return $this->name;
    }

    /**
     * @param WalletCreated $event
     * @throws \App\Wallet\Domain\DomainException
     */
    protected function applyWalletCreated(WalletCreated $event)
    {
        $this->walletId = $event->walletId();
        $this->name = new WalletName($event->walletName());
    }

    /**
     * @param WalletRenamed $event
     * @throws \App\Wallet\Domain\DomainException
     */
    protected function applyWalletRenamed(WalletRenamed $event)
    {
        $this->name = new WalletName($event->walletName());
    }

    /**
     * @param AddedMoneyToWallet $event
     */
    protected function applyAddedMoneyToWallet(AddedMoneyToWallet $event)
    {
        $this->balance += $event->amount();
    }

    /**
     * @param WithdrawnMoneyFromWallet $event
     */
    protected function applyWithdrawnMoneyFromWallet(WithdrawnMoneyFromWallet $event)
    {
        $this->balance -= $event->amount();
    }
}