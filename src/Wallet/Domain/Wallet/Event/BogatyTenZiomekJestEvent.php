<?php


namespace App\Wallet\Domain\Wallet\Event;


use App\Common\Aggregate\AggregateChanged;

class BogatyTenZiomekJestEvent extends AggregateChanged
{
    public function eventName(): string
    {
        return 'wallet-bogaty-ten-ziomek';
    }
}