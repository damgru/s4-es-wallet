<?php

namespace App\Wallet\Domain\Wallet\Event;


use App\Common\Aggregate\AggregateChanged;
use App\Common\Uuid;

class WalletRenamed extends AggregateChanged
{
    const EVENT_NAME = 'wallet-renamed';

    public static function with(Uuid $walletId, string $name)
    {
        return new static($walletId->toString(), ['name' => $name]);
    }

    public function walletId() : Uuid
    {
        return Uuid::fromString($this->aggregateId());
    }

    /**
     * @throws \App\Wallet\Domain\DomainException
     */
    public function walletName(): string
    {
        return $this->payload['name'];
    }

    public function eventName(): string
    {
        return self::EVENT_NAME;
    }
}