<?php

namespace App\Wallet\Domain\Wallet\Event;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Uuid;

class AddedMoneyToWallet extends AggregateChanged
{
    const EVENT_NAME = 'wallet-added-money';


    public static function with(string $walletId, $amount)
    {
        return new static($walletId, [
            'amount' => $amount
        ]);
    }

    public function walletId() : Uuid
    {
        return Uuid::fromString($this->aggregateId);
    }

    public function amount() : float
    {
        return (float)$this->payload['amount'];
    }

    public function eventName(): string
    {
        return self::EVENT_NAME;
    }
}