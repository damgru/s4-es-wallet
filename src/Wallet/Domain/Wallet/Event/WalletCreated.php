<?php

namespace App\Wallet\Domain\Wallet\Event;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Uuid;

class WalletCreated extends AggregateChanged
{
    const EVENT_NAME = 'wallet-created';

    /**
     * WalletCreated constructor.
     * @param string $walletId
     * @param string $name
     * @return WalletCreated
     */
    public static function with(string $walletId, string $name)
    {
        return new static($walletId, [
            'name' => $name
        ]);
    }

    public function walletId() : Uuid
    {
        return Uuid::fromString($this->aggregateId);
    }

    /**
     * @return string
     */
    public function walletName(): string
    {
        return $this->payload['name'];
    }

    public function eventName(): string
    {
        return self::EVENT_NAME;
    }
}