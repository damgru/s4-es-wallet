<?php

namespace App\Wallet\Domain;

use Throwable;

class DomainException extends \Exception
{
    private $debugData;

    public function __construct(string $message = "", array $debugData = [], Throwable $previous = null)
    {
        $this->debugData = $debugData;
        parent::__construct($message, 0, $previous);
    }

    /**
     * @return array
     */
    public function getDebugData(): array
    {
        return $this->debugData;
    }
}