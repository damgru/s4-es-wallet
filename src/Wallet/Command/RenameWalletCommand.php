<?php
namespace App\Wallet\Command;

use App\Common\Command;

class RenameWalletCommand implements Command
{
    /** @var string */
    private $walletId;
    /** @var string */
    private $newName;

    /**
     * RenameWalletCommand constructor.
     * @param string $walletId
     * @param string $newName
     */
    public function __construct(string $walletId, string $newName)
    {
        $this->walletId = $walletId;
        $this->newName = $newName;
    }

    /**
     * @return string
     */
    public function getWalletId(): string
    {
        return $this->walletId;
    }

    /**
     * @return string
     */
    public function getNewName(): string
    {
        return $this->newName;
    }


}