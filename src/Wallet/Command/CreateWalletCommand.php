<?php

namespace App\Wallet\Command;

use App\Common\Command;
use App\Common\Uuid;

class CreateWalletCommand implements Command
{
    /** @var string */
    public $name;

    /** @var Uuid */
    public $uuid;

    public static function with(Uuid $uuid, string $name): self
    {
        $cmd = new static();
        $cmd->name = $name;
        $cmd->uuid = $uuid;
        return $cmd;
    }
}