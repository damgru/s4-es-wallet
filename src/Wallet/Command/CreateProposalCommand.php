<?php


namespace App\Wallet\Command;


use App\Common\Command;
use App\Common\Uuid;

class CreateProposalCommand implements Command
{
    /** @var Uuid */
    private $uuid;
    /** @var Uuid */
    private $walletUuid;
    /** @var float */
    private $amount;
    /** @var string */
    private $desc;

    /**
     * CreateProposalCommand constructor.
     * @param Uuid $uuid
     * @param Uuid $walletUuid
     * @param float $amount
     * @param string $desc
     */
    public function __construct(Uuid $uuid, Uuid $walletUuid, float $amount, string $desc)
    {
        $this->uuid = $uuid;
        $this->walletUuid = $walletUuid;
        $this->amount = $amount;
        $this->desc = $desc;
    }

    /**
     * @return Uuid
     */
    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    /**
     * @return Uuid
     */
    public function getWalletUuid(): Uuid
    {
        return $this->walletUuid;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getDesc(): string
    {
        return $this->desc;
    }
}