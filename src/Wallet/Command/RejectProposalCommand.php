<?php


namespace App\Wallet\Command;


use App\Common\Command;
use App\Common\Uuid;

class RejectProposalCommand implements Command
{
    /** @var Uuid */
    private $uuid;
    /** @var string */
    private $reason;

    /**
     * CreateProposalCommand constructor.
     * @param Uuid $uuid
     * @param string $reason
     */
    public function __construct(Uuid $uuid, string $reason = '')
    {
        $this->uuid = $uuid;
        $this->reason = $reason;
    }

    /**
     * @return Uuid
     */
    public function getUuid(): Uuid
    {
        return $this->uuid;
    }

    /**
     * @return string
     */
    public function reason(): string
    {
        return $this->reason;
    }
}