<?php

namespace App\Wallet\Infrastructure\Repository;

use App\Common\Aggregate\AggregateRepository;
use App\Common\Uuid;
use App\Wallet\Domain\Wallet\Wallet;
use App\Wallet\Domain\Wallet\WalletRepository;

class EventSourcedWalletRepository extends AggregateRepository implements WalletRepository
{
    /**
     * @param Uuid $walletId
     * @return Wallet
     * @throws \App\Common\Aggregate\AggregateException
     */
    public function getById(Uuid $walletId): Wallet
    {
        return $this->loadAggregateRoot(Wallet::class, $walletId);
    }

    public function save(Wallet $wallet): void
    {
        $this->saveAggregateRoot($wallet);
    }
}