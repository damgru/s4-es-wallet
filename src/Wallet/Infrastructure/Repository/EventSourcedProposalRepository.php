<?php


namespace App\Wallet\Infrastructure\Repository;


use App\Common\Aggregate\AggregateRepository;
use App\Common\Uuid;
use App\Wallet\Domain\Proposal\Proposal;
use App\Wallet\Domain\Proposal\ProposalRepository;

class EventSourcedProposalRepository extends AggregateRepository implements ProposalRepository
{
    /**
     * @param Uuid $uuid
     * @return Proposal
     * @throws \App\Common\Aggregate\AggregateException
     */
    public function getById(Uuid $uuid): Proposal
    {
        return $this->loadAggregateRoot(Proposal::class, $uuid);
    }

    public function save(Proposal $proposal): void
    {
        $this->saveAggregateRoot($proposal);
    }
}