<?php


namespace App\Wallet\Infrastructure\EventFactory;


use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\EventFactory;
use App\Wallet\Domain\Wallet\Event\AddedMoneyToWallet;
use App\Wallet\Domain\Wallet\Event\WalletCreated;
use App\Wallet\Domain\Wallet\Event\WalletRenamed;
use App\Wallet\Domain\Wallet\Event\WithdrawnMoneyFromWallet;

class StaticEventFactory implements EventFactory
{
    public function createFromName(
        string $eventName,
        string $aggregateId,
        array $payload,
        \DateTimeImmutable $created = null,
        int $version = 0,
        string $category = ''
    ): AggregateChanged
    {
        switch ($eventName) {

            case AddedMoneyToWallet::EVENT_NAME:
                return new AddedMoneyToWallet($aggregateId, $payload, $created, $version,$category);

            case WalletCreated::EVENT_NAME:
                return new WalletCreated($aggregateId, $payload, $created, $version, $category);

            case WalletRenamed::EVENT_NAME:
                return new WalletRenamed($aggregateId, $payload, $created, $version, $category);

            case WithdrawnMoneyFromWallet::EVENT_NAME:
                return new WithdrawnMoneyFromWallet($aggregateId, $payload, $created, $version, $category);
        }
    }
}