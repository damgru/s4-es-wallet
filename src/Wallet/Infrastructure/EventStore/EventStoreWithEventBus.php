<?php

namespace App\Wallet\Infrastructure\EventStore;


use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\EventStore;
use App\Common\EventBus;
use App\Common\Uuid;

class EventStoreWithEventBus implements EventStore
{
    /** @var EventStore */
    private $eventStore;

    /** @var EventBus */
    private $eventBus;


    /**
     * EventStoreWithEventBus constructor.
     * @param EventStore $eventStore
     * @param EventBus $eventBus
     */
    public function __construct(EventStore $eventStore, EventBus $eventBus)
    {
        $this->eventStore = $eventStore;
        $this->eventBus = $eventBus;
    }

    /**
     * @param AggregateChanged[] $events
     */
    public function add(\App\Common\Aggregate\AggregateChanged ...$events): void
    {
        $this->eventStore->add(...$events);
        foreach ($events as $event) {
            $this->eventBus->dispatch($event);
        }
    }

    /**
     * @param \App\Common\Uuid $aggregateId
     * @return iterable<AggregateChanged>
     */
    public function getEvents(Uuid $aggregateId): iterable
    {
        return $this->eventStore->getEvents($aggregateId);
    }

    /**
     * @return iterable<AggregateChanged>
     */
    public function getAllEvents(): iterable
    {
        return $this->eventStore->getAllEvents();
    }

    /**
     * @param Uuid $aggregateId
     * @param int $version
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsFromVersion(Uuid $aggregateId, int $version, $limit = 0): iterable
    {
        return $this->eventStore->getEventsFromVersion($aggregateId, $version, $limit);
    }
}