<?php

namespace App\Wallet\Infrastructure\EventStore;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\EventStore;
use App\Common\Uuid;

class InMemoryEventStore implements EventStore
{
    /** @var AggregateChanged[] */
    private $events = [];

    /**
     * InMemoryEventStore constructor.
     * @param AggregateChanged[] $events
     */
    public function __construct(AggregateChanged ...$events)
    {
        $this->events = $events;
    }

    /**
     * @param AggregateChanged $events
     */
    public function add(AggregateChanged ...$events): void
    {
        $this->events = array_merge($this->events, $events);
    }


    /**
     * @param Uuid $aggregateId
     * @return iterable<AggregateChanged>
     */
    public function getEvents(Uuid $aggregateId): iterable
    {
        $uuid = $aggregateId->toString();
        foreach ($this->events as $event) {
            if ($event->aggregateId() === $uuid) {
                yield $event;
            }
        }
    }

    /**
     * @return iterable<AggregateChanged>
     */
    public function getAllEvents(): iterable
    {
        return $this->events;
    }

    /**
     * @param Uuid $aggregateId
     * @param int $version
     * @param int $limit
     * @return iterable<AggregateChanged>
     */
    public function getEventsFromVersion(Uuid $aggregateId, int $version, $limit = 0): iterable
    {
        $uuid = $aggregateId->toString();
        $limit = empty($limit) ? -1 : $limit;
        foreach ($this->events as $event) {
            if ($event->aggregateId() === $uuid && $event->version() > $version) {
                yield $event;
                $limit--;
            }
            if ($limit == 0) break;
        }
    }
}