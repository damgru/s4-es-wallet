<?php

namespace App\Wallet\Infrastructure\EventStore;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\EventFactory;
use App\Common\Aggregate\EventStore;
use App\Common\Uuid;
use Doctrine\DBAL\Driver\Connection;
use Doctrine\DBAL\ParameterType;

class DbalEventStore implements EventStore
{
    /** @var EventFactory */
    private $eventFactory;
    /** @var Connection */
    private $connection;

    /**
     * DbalEventStore constructor.
     * @param EventFactory $eventFactory
     */
    public function __construct(Connection $connection, EventFactory $eventFactory)
    {
        $this->eventFactory = $eventFactory;
        $this->connection = $connection;
    }

    /**
     * @param AggregateChanged[] $events
     */
    public function add(AggregateChanged ...$events): void
    {
        $sql = <<<SQL
            INSERT INTO events (aggregate_id, event_name, payload, created, version, category) 
            VALUES (?,?,?,?,?,?)
SQL;
        $stmt = $this->connection->prepare($sql);

        foreach ($events as $event)
        {
            $stmt->execute([
                $event->aggregateId(),
                $event->eventName(),
                json_encode($event->payload()),
                $event->created()->format('Y-m-d H:i:s'),
                $event->version(),
                $event->category()
            ]);
        }
    }

    /**
     * @param \App\Common\Uuid $aggregateId
     * @return iterable<AggregateChanged>
     */
    public function getEvents(Uuid $aggregateId): iterable
    {
        $sql = <<<SQL
SELECT * FROM events WHERE aggregate_id = ? ORDER BY created ASC, version ASC;
SQL;
        $stmt = $this->connection->prepare($sql);

        $stmt->execute([
            $aggregateId->toString()
        ]);
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            yield $this->eventFactory->createFromName(
                $row['event_name'],
                $row['aggregate_id'],
                json_decode($row['payload'], true),
                new \DateTimeImmutable($row['created']),
                $row['version'],
                $row['category']
            );
        }
    }

    /**
     * @return iterable<AggregateChanged>
     */
    public function getAllEvents(): iterable
    {
        $sql = <<<SQL
SELECT * 
FROM events 
ORDER BY created ASC, version ASC;
SQL;
        $stmt = $this->connection->prepare($sql);

        $stmt->execute();
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            yield $this->eventFactory->createFromName(
                $row['event_name'],
                $row['aggregate_id'],
                json_decode($row['payload'], true),
                new \DateTimeImmutable($row['created']),
                $row['version'],
                $row['category']
            );
        }
    }

    /**
     * @param Uuid $aggregateId
     * @param int $version
     * @param int $limit
     * @return iterable<AggregateChanged>
     * @throws \Exception
     */
    public function getEventsFromVersion(Uuid $aggregateId, int $version, $limit = 10): iterable
    {
        $sql = <<<SQL
SELECT * 
FROM events 
WHERE aggregate_id = :aggregate_id 
  AND version >= :version 
ORDER BY created ASC, version ASC
LIMIT :limit
SQL;

        $stmt = $this->connection->prepare($sql);
        $stmt->bindParam('aggregate_id', $aggregateId->toString());
        $stmt->bindParam('version', $version);
        $stmt->bindParam('limit', $limit);

        $stmt->execute();
        $rows = $stmt->fetchAll();
        foreach ($rows as $row) {
            yield $this->eventFactory->createFromName(
                $row['event_name'],
                $row['aggregate_id'],
                json_decode($row['payload'], true),
                new \DateTimeImmutable($row['created']),
                $row['version'],
                $row['category']
            );
        }
    }
}