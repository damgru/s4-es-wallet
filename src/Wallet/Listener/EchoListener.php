<?php
/**
 * Created by PhpStorm.
 * User: dgrudzien
 * Date: 25.03.19
 * Time: 14:06
 */

namespace App\Wallet\Listener;

use App\Common\Aggregate\AggregateChanged;

class EchoListener
{
    public function __invoke(AggregateChanged $event)
    {
        echo "###\nECHO LISTENER\n";
        echo $event->eventName()."\n";
        echo $event->aggregateId()."\n";
        echo "\n###\n";
    }
}