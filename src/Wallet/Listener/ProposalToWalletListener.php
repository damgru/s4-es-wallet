<?php


namespace App\Wallet\Listener;


use App\Common\Aggregate\AggregateChanged;
use App\Common\Uuid;
use App\Wallet\Domain\Proposal\Event\ProposalAccepted;
use App\Wallet\Domain\Proposal\ProposalRepository;
use App\Wallet\Domain\Wallet\WalletRepository;

class ProposalToWalletListener
{
    /** @var WalletRepository */
    private $walletRepository;
    /** @var ProposalRepository */
    private $proposalRepository;

    /**
     * ProposalToWalletListener constructor.
     * @param WalletRepository $walletRepository
     * @param ProposalRepository $proposalRepository
     */
    public function __construct(WalletRepository $walletRepository, ProposalRepository $proposalRepository)
    {
        $this->walletRepository = $walletRepository;
        $this->proposalRepository = $proposalRepository;
    }

    public function __invoke(AggregateChanged $event)
    {
        if (get_class($event) === ProposalAccepted::class) {
            $proposal = $this->proposalRepository->getById(Uuid::fromString($event->aggregateId()));
            $wallet = $this->walletRepository->getById($proposal->walletUuid());
            $wallet->addMoney($proposal->amount());
            $this->walletRepository->save($wallet);
        }
    }
}