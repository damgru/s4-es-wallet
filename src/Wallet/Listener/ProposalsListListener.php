<?php


namespace App\Wallet\Listener;

use App\Common\Aggregate\AggregateChanged;
use App\Common\Aggregate\ApplyByClassShortNameTrait;

class ProposalsListListener
{
    use ApplyByClassShortNameTrait;

    public function __invoke(AggregateChanged $event)
    {
        $this->apply($event);
    }


}