<?php
/**
 * Created by PhpStorm.
 * User: dgrudzien
 * Date: 27.03.19
 * Time: 11:59
 */

namespace App\Wallet\Domain\Wallet;

use PHPUnit\Framework\TestCase;

class WalletNameTest extends TestCase
{
    /**
     * @param $name
     * @dataProvider goodNames
     * @throws \App\Wallet\Domain\DomainException
     */
    public function testGoodWalletNames($name)
    {
        $wallet = new WalletName($name);
        $this->assertEquals($wallet->toString(), $name);
    }

    /**
     * @param $name
     * @dataProvider wrongNames
     * @expectedException \App\Wallet\Domain\DomainException
     */
    public function testWrongWalletNames($name)
    {
        $wallet = new WalletName($name);
    }

    public function wrongNames()
    {
        return [
            ['W@llet'],
            [' '],
            [''],
            [' hehe '],
            ['!@3'],
        ];
    }

    public function goodNames()
    {
        return [
            ['Wallet Name'],
            ['Coś innego'],
            ['Zażółć gęślą jaźń'],
            ['Hejo'],
        ];
    }
}
