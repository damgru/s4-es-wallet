<?php
namespace App\Wallet\Domain\Wallet;

use App\Common\Uuid;
use App\Wallet\Domain\DomainException;
use App\Wallet\Domain\Wallet\Event\AddedMoneyToWallet;
use App\Wallet\Domain\Wallet\Event\WalletCreated;
use App\Wallet\Domain\Wallet\Event\WalletRenamed;
use App\Wallet\Domain\Wallet\Event\WithdrawnMoneyFromWallet;
use PHPUnit\Framework\TestCase;

class WalletTest extends TestCase
{
    /**
     * @throws DomainException
     */
    public function testWalletCreated()
    {
        $uuid = Uuid::fromString('9ffd78d6-8cb1-4237-8143-c4b96f84e0e9');
        $walletName = new WalletName('Sample Wallet');
        $wallet = Wallet::create($uuid, $walletName);

        $this->assertEquals($walletName, $wallet->name());
        $this->assertEquals($uuid, $wallet->walletId());
        $this->assertEquals($wallet->balance(), 0);
    }

    /**
     * @throws DomainException
     */
    public function testRenameWallet()
    {
        $uuid = Uuid::fromString('9ffd78d6-8cb1-4237-8143-c4b96f84e0e9');
        $walletName = new WalletName('Sample Wallet');
        $wallet = Wallet::create($uuid, $walletName);

        $newWalletName = new WalletName('hehe');
        $wallet->renameTo($newWalletName);

        $this->assertEquals($wallet->name(), $newWalletName);
    }

    /**
     * @throws DomainException
     */
    public function testAddMoney()
    {
        $uuid = Uuid::fromString('9ffd78d6-8cb1-4237-8143-c4b96f84e0e9');
        $walletName = new WalletName('Sample Wallet');
        $wallet = Wallet::create($uuid, $walletName);

        $wallet->addMoney(100);
        $this->assertEquals($wallet->balance(), 100);
    }

    public function testMoneyAdded()
    {
        $uuid = Uuid::fromString('9ffd78d6-8cb1-4237-8143-c4b96f84e0e9');

        /** @var Wallet $wallet */
        $wallet = Wallet::fromEvents([
            WalletCreated::with($uuid, 'Sample Wallet'),
            AddedMoneyToWallet::with($uuid, 100),
            AddedMoneyToWallet::with($uuid, 150),
        ]);

        $this->assertEquals(250, $wallet->balance());
    }

    public function testMoneyWithdrawn()
    {
        $uuid = Uuid::fromString('9ffd78d6-8cb1-4237-8143-c4b96f84e0e9');

        /** @var Wallet $wallet */
        $wallet = Wallet::fromEvents([
            WalletCreated::with($uuid, 'Sample Wallet'),
            AddedMoneyToWallet::with($uuid, 100),
            WithdrawnMoneyFromWallet::with($uuid, 50),
        ]);

        $this->assertEquals(50, $wallet->balance());
    }

    public function testMoneyAddMany()
    {
        $uuid = Uuid::fromString('9ffd78d6-8cb1-4237-8143-c4b96f84e0e9');
        $walletName = 'Sample Wallet';

        $events = [
            WalletCreated::with($uuid->toString(), $walletName)
        ];

        $times = 1000;
        $amount = 0.01;
        for($i = 0 ; $i < $times ; $i++)
        {
            $events[] = AddedMoneyToWallet::with($uuid, $amount);
        }

        /** @var Wallet $wallet */
        $wallet = Wallet::fromEvents($events);
        $this->assertEquals($amount*$times, $wallet->balance());
    }

    public function testWalletRenamed()
    {
        $uuid = Uuid::fromString('9ffd78d6-8cb1-4237-8143-c4b96f84e0e9');
        $walletName = 'Sample Wallet';
        $newWalletName = 'Another Name';

        /** @var Wallet $wallet */
        $wallet = Wallet::fromEvents([
            WalletCreated::with($uuid, $walletName),
            WalletRenamed::with($uuid, $newWalletName)
        ]);

        $this->assertEquals($newWalletName, $wallet->name()->toString());
    }
}
