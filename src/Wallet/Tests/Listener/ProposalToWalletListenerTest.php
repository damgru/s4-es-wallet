<?php

namespace App\Wallet\Listener;

use App\Common\Uuid;
use App\Wallet\Domain\Proposal\Event\ProposalAccepted;
use App\Wallet\Domain\Proposal\Proposal;
use App\Wallet\Domain\Wallet\Wallet;
use App\Wallet\Domain\Wallet\WalletName;
use App\Wallet\Infrastructure\EventStore\InMemoryEventStore;
use App\Wallet\Infrastructure\Repository\EventSourcedProposalRepository;
use App\Wallet\Infrastructure\Repository\EventSourcedWalletRepository;
use PHPUnit\Framework\TestCase;

class ProposalToWalletListenerTest extends TestCase
{
    public function testMoneyAddedAfterProposalIsAccepted()
    {
        $eventStore = new InMemoryEventStore();
        $walletRepo = new EventSourcedWalletRepository($eventStore);
        $walletUuid = Uuid::generate();
        $wallet = Wallet::create($walletUuid, new WalletName('test'));
        $walletRepo->save($wallet);

        $proposalRepo = new EventSourcedProposalRepository($eventStore);
        $proposalUuid = Uuid::generate();
        $proposal = Proposal::createNewProposal($proposalUuid, $walletUuid, 10, 'asdf');
        $proposalRepo->save($proposal);

        $listener = new ProposalToWalletListener($walletRepo, $proposalRepo);
        $event = ProposalAccepted::with($proposalUuid->toString());

        $listener($event);

        $walletAfterChange = $walletRepo->getById($walletUuid);
        $this->assertEquals(10, $walletAfterChange->balance());
    }

}
