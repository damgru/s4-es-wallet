<?php

namespace App\Controller;

use App\Common\CommandBus;
use App\Common\Uuid;
use App\Wallet\Command\CreateWalletCommand;
use App\Wallet\Domain\Wallet\WalletRepository;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StartController extends AbstractController
{
    /**
     * @Route("/", name="start")
     */
    public function startAction(CommandBus $commandBus, WalletRepository $walletRepository)
    {
        $uuid = Uuid::generate();
        $commandBus->handle(CreateWalletCommand::with($uuid, 'Test'));
        echo '<pre>';

        var_dump($walletRepository->getById($uuid));
        return new Response('');
    }

    /**
     * @Route("/dbal", name="dbal_test")
     */
    public function testDbal(Connection $connection)
    {
        $sql = $connection->prepare('SELECT * FROM swpirb_users LIMIT ?');
        $sql->execute([5]);
        $users = $sql->fetchAll();

        var_dump($users);

        return new Response('');
    }
}