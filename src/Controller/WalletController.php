<?php

namespace App\Controller;

use App\Common\CommandBus;
use App\Common\Uuid;
use App\Wallet\Command\CreateProposalCommand;
use App\Wallet\Command\CreateWalletCommand;
use App\Wallet\Command\RenameWalletCommand;
use App\Wallet\Domain\Wallet\WalletRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class WalletController extends AbstractController
{
    /** @var CommandBus */
    private $commandBus;

    /**
     * WalletController constructor.
     * @param CommandBus $commandBus
     */
    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @Route("/wallet/create", name="wallet-create")
     */
    public function createAction()
    {
        $uuid = Uuid::generate();
        $command = CreateWalletCommand::with($uuid, 'Hello');
        $this->commandBus->handle($command);
        return new Response($uuid->toString());
    }

    /**
     * @Route("/wallet/{id}", name="wallet-show")
     * @param $id
     * @param WalletRepository $walletRepository
     * @return Response
     */
    public function showWalletAction($id, WalletRepository $walletRepository)
    {
        $wallet = $walletRepository->getById(Uuid::fromString($id));
        var_dump($wallet);
        return new Response('');
    }

    /**
     * @Route("/wallet/{id}/rename/{newName}", name="wallet-rename")
     * @param $id
     * @param WalletRepository $walletRepository
     * @return Response
     */
    public function renameWalletAction($id, $newName, CommandBus $commandBus)
    {
        $commandBus->handle(new RenameWalletCommand($id, $newName));
        return new Response($id);
    }

    /**
     * @Route("/wallet/{walletUuid}/proposal/create", name="proposal-create")
     */
    public function createProposal(Request $request, $walletUuid)
    {
        $uuid = Uuid::generate();
        $walletUuid = Uuid::fromString($walletUuid);

        $this->commandBus->handle(new CreateProposalCommand($uuid, $walletUuid, $request->get('amount'), $request->get('description')));

        return new Response($uuid->toString());
    }

}